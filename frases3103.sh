#!/bin/bash
printf "\033[0;31mÉ sempre divertido fazer o impossível. - Walt Disney \n"
sleep 1
printf "\033[0;32mNada acontece a menos que sonhemos abtes. - Carl Sandburg \n"
sleep 1
printf "\033[0;33mO sucesso é ir de fracasso em fracasso sem perder o entusiasmo. - Autor Desconhecido \n"
sleep 1
printf "\033[0;34mMesmo que algo pareça difícil, nunca desista antes de tentar. - Rose Nere \n"
sleep 1
printf "\033[0;35mUma atitude vitoriosa é meio caminho andado para o sucesso. - Arthur Riedel \n"
sleep 1
printf "\033[0;37m"
