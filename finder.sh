#!/bin/bash

printf "Digite o nome de três diretórios...\n"
read f1 f2 f3

cd ~/$f1
printf "Mostrando os arquivos de $(pwd):\n"
ls -l
printf "Pressione Enter para continuar\n"
read

cd ~/$f2
printf "Mostrando os arquivos de $(pwd):\n"
ls -l
printf "Pressione Enter para continuar\n"
read

cd ~/$f3
printf "Mostrando os arquivos de $(pwd):\n"
ls -l
printf "Pressione Enter para finalizar\n"
read
